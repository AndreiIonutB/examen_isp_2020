import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Campuri extends JFrame {
    JTextField txt1;
    JTextField txt2;
    JButton btn;

    Campuri(){
        setTitle("Transfer Word");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        init();
        setSize(240,170);
        setVisible(true);
    }

    public void init(){
        
        this.setLayout(null);
        int width=200;int height = 20;

        txt1 = new JTextField();
        txt1.setBounds(10,30,width, height);
        txt2 = new JTextField();
        txt2.setBounds(10,60,width, height);
        txt2.setEditable(false);


        btn = new JButton("Transfera Cuvantul");
        btn.setBounds(10,90,width, height);

        btn.addActionListener(new ApasareButon());

        add(txt1);add(btn);
        add(txt2);

    }

    public static void main(String[] args) {
        new Campuri();
    }

    class ApasareButon implements ActionListener {

        public void actionPerformed(ActionEvent e) {

            String textul = Campuri.this.txt1.getText();
            Campuri.this.txt1.setText(" ");
            Campuri.this.txt2.setText(textul);

        }

    }



    }


